<?php

/**
 * @file
 * Main module file containing hooks.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_help().
 */
function session_node_access_help($route_name, RouteMatchInterface $route_match) {
  return $route_name === 'help.page.simple_sitemap'
    ? check_markup(file_get_contents(__DIR__ . '/README.md'))
    : NULL;
}

/**
 * Implements hook_node_access().
 *
 * Grants access to users to view/edit/delete a node (depending
 * on settings) in case they created it in their current session.
 */
function session_node_access_node_access(NodeInterface $node, $op, AccountInterface $account) {
  return session_node_access_user_node_access($node, $op, $account)
    ? AccessResult::allowed()
    : AccessResult::neutral();
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 *
 * Makes sure that content created by users gets referenced in their
 * session variable to be checked for in hook_node_access.
 */
function session_node_access_node_insert(NodeInterface $node) {
  $config = \Drupal::config('session_node_access.settings');

  if (!empty($config->get('node_types')[$node->getType()])) {
    foreach (\Drupal::currentUser()->getRoles() as $role_id) {
      if (!empty($config->get('user_roles')[$role_id])) {
        $_SESSION['session_node_access']['nodes_created'][] = $node->id();
        break;
      }
    }
  }
}

/**
 * Checks if currently logged-in user has access to a node.
 *
 * @param \Drupal\node\NodeInterface $node
 *   The node to be checked if accessible by currently logged-in user.
 * @param string $op
 *   The user operation for which to check access (view/update/delete).
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The user to be checked for access to the node.
 *   If not provided, current user is taken.
 *
 * @return bool
 *   True if user has access, false if they do not.
 */
function session_node_access_user_node_access(NodeInterface $node, $op, AccountInterface $account) {
  if (!isset($_SESSION['session_node_access']['nodes_created'])) {
    return FALSE;
  }

  $config = \Drupal::config('session_node_access.settings');

  // Check if settings allow granting permissions to this node.
  if (!empty($config->get('operations')[$op])) {
    if (!empty($config->get('node_types')[$node->getType()])) {
      foreach ($account->getRoles() as $role_id) {
        if (!empty($config->get('user_roles')[$role_id])) {
          if (empty($config->get('published')) || $node->isPublished()) {
            if (in_array($node->id(), $_SESSION['session_node_access']['nodes_created'], FALSE)) {
              return TRUE;
            }
          }
        }
      }
    }
  }

  return FALSE;
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function session_node_access_user_insert(EntityInterface $entity) {
  if (empty($_SESSION['session_node_access']['nodes_created'])) {
    return;
  }

  if (!empty(\Drupal::config('session_node_access.settings')->get('change_ownership'))) {

    /** @var \Drupal\node\NodeInterface[] $nodes */
    $nodes = \Drupal::entityTypeManager()->getStorage('node')
      ->loadMultiple($_SESSION['session_node_access']['nodes_created']);
    foreach ($nodes as $node) {
      /** @var \Drupal\user\UserInterface  $entity */
      $node->setOwner($entity)->save();
      unset($_SESSION['session_node_access']['nodes_created'][$node->id()]);
    }
  }
}
